dnl Copyright (c) 2009  Daniel Elstner <daniel.kitta@gmail.com>
dnl This file is part of librsvgmm.

_CONVERSION(`const guint8*', `const guchar*', `$3')
_CONVERSION(`RsvgHandle*',`Glib::RefPtr<Handle>',`Glib::wrap($3)')
_CONVERSION(`DimensionData&',`RsvgDimensionData*',`&($3)')
_CONVERSION(`PositionData&',`RsvgPositionData*',`&($3)')
_CONVERSION(`const Cairo::RefPtr<Cairo::Context>&',`cairo_t*',`($3)->cobj()')
