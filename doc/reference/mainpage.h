/*!
@mainpage librsvgmm Reference Manual

@section description Description

librsvg is a component used within software applications to enable support for
SVG-format scalable graphics.  In contrast to raster formats, scalable vector
graphics provide users and artists a way to create, view, and provide imagery
that is not limited to the pixel or dot density that an output device is
capable of.

The librsvgmm C++ binding provides a C++ interface on top of the librsvg
C library.

@section overview Overview

Due to its limited scope, the librsvgmm API is very small.  SVG images are
represented by Rsvg::Handle objects, loaded either by filename or from an
in-memory buffer.  The Rsvg::Handle class also provides methods to access
image properties and for rendering through cairo.  A number of methods throw
Rsvg::Error exceptions on failure to access or interpret an external resource
at runtime.

Before using any other part of the library, Rsvg::init() must be called once
to initialize librsvgmm, typically from an application's @c %main() function.
The Rsvg::term() function may be called after use in order to release some
of the resources allocated during initialization, which is primarily useful
as an aid to memory allocation profilers.

@note
The convenience methods for rendering to a @c GdkPixbuf, which are still
available in the librsvg C API, are not included in the C++ binding.  These
methods have been deliberately omitted in order to avoid an otherwise
unnecessary dependency on gtkmm.  The basic functionality of loading an SVG
into a @c GdkPixbuf is already available through the rsvg pixbuf loader module.
*/
