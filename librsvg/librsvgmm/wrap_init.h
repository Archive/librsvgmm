/* Copyright (c) 2009  Daniel Elstner <daniel.kitta@gmail.com>
 *
 * This file is part of librsvgmm.
 *
 * librsvgmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * librsvgmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBRSVGMM_WRAP_INIT_H_INCLUDED
#define LIBRSVGMM_WRAP_INIT_H_INCLUDED

namespace Rsvg { void wrap_init(); }

#endif /* !LIBRSVGMM_WRAP_INIT_H_INCLUDED */
